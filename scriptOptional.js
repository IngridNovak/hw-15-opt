'use strict'
//1. Рекурсія це коли функція викликає саму себе.Деякі задачі самі по собі рекурсивні, наприклад алгоритми для обчислення факторіалу
//або чисел Фібоначчі. Це рекурсивні дії, оскільки ми обчислюємо наступне число, використовуючи значення попереднього.
function factorial(n){
    if (n==1) return 1;
    else return n*factorial(n-1);
};
let userNum=+prompt('Введіть число:');
while(isNaN(userNum) || !userNum){
    userNum=+prompt('Введіть число:',userNum);
};
alert(factorial(userNum));
